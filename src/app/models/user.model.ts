export interface User {
  id: number;
  fullName: string;
  password: string;
  email: string;
  userRole: string;
  bio: string;
  avatar: string;
  rank: number;
  pricePerHour: number;
  address: string;
  birthDate: Date;
  gender: string;
  phone: string;
  balance: number;
  status: number;
}
