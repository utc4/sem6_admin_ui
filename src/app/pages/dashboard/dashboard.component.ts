import { Component, OnInit } from '@angular/core';
import {StatisticService} from '../../services/statistic.service';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  overview: any;
  userStructureGraph: any;
  financeGraph: any;

  constructor(private statisticService: StatisticService) { }

  ngOnInit(): void {
    this.statisticService.statOverview()
      .subscribe(data => this.overview = data);
    this.statisticService.statUserStructure()
      .subscribe(data => {
        this.userStructureGraph = {
          type: 'pie',
          data: {
            labels: [
              'Visitors',
              'Collaborators',
            ],
            datasets: [
              {
                label: '',
                data: [
                  data.visitors,
                  data.collaborators,
                ],
                backgroundColor: [
                  'rgb(54, 162, 235)',
                  'rgb(255, 205, 86)',
                ],
              },
            ],
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
          },
        };
      });
    this.statisticService.statRevenue()
      .subscribe(data => {
        this.financeGraph = {
          type: 'bar',
          data: {
            labels: data.map(d => `${d.year}/${d.month}`),
            datasets: [
              {
                label: 'Revenue',
                data: data.map(d => d.amount),
                backgroundColor: 'rgba(54, 162, 235, 0.5)',
                borderColor: 'rgb(54, 162, 235)',
                borderWidth: 1,
              },
            ],
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
          },
        };
      });
  }
}
