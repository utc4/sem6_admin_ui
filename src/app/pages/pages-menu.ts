import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Accounts',
    icon: 'people-outline',
    link: '/pages/users',
  },
  {
    title: 'Topics',
    icon: 'layers-outline',
    link: '/pages/topics',
  },
];
