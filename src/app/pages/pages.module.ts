import { NgModule } from '@angular/core';
import {
  NbButtonModule,
  NbCardModule,
  NbIconModule, NbInputModule,
  NbMenuModule,
  NbSelectModule,
  NbTooltipModule
} from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { UserComponent } from './user/user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { UserDetailsComponent } from './user-details/user-details.component';
import {ChartModule} from 'angular2-chartjs';
import { TopicComponent } from './topic/topic.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbCardModule,
    NbButtonModule,
    MatTableModule,
    MatPaginatorModule,
    NbSelectModule,
    NbButtonModule,
    NbIconModule,
    ChartModule,
    NbTooltipModule,
    FormsModule,
    NbInputModule,
  ],
  declarations: [
    PagesComponent,
    UserComponent,
    DashboardComponent,
    NotFoundComponent,
    UserDetailsComponent,
    TopicComponent,
  ],
})
export class PagesModule {
}
