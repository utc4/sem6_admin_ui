import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {PageResponse} from '../../dto/page-response';
import {User} from '../../models/user.model';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'ngx-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  user: PageResponse<User[]>;
  displayedColumns = ['id', 'fullName', 'email', 'userRole', 'phone', 'gender', 'status', 'actions'];

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.list()
      .subscribe(rs => this.user = rs);
  }

  pageChange($event: PageEvent) {
    this.userService.list($event.pageIndex + 1, $event.pageSize)
      .subscribe(rs => this.user = rs);
  }

  roleHighlight(userRole: string) {
    switch (userRole) {
      case 'ADMIN': return 'highlight-admin';
      case 'VISITOR': return 'highlight-visitor';
      case 'COLLABORATOR': return 'highlight-collab';
    }
  }

  statusHighlight(status: number) {
    return status === 1 ? 'highlight-active' : 'highlight-inactive';
  }
}
