import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RestResponse} from '../dto/rest-response';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class StatisticService {

  constructor(private http: HttpClient) { }

  statRevenue(): Observable<any> {
    return this.http
      .get<RestResponse<any>>(`${environment.apiUrl}/statistic/revenue`)
      .pipe(map(res => res.data));
  }

  statFinance(uid: number): Observable<any> {
    return this.http
      .get<RestResponse<any>>(`${environment.apiUrl}/statistic/finance/${uid}`)
      .pipe(map(res => res.data));
  }

  statOverview(): Observable<any> {
    return this.http
      .get<RestResponse<any>>(`${environment.apiUrl}/statistic/overview`)
      .pipe(map(res => res.data));
  }

  statUserStructure(): Observable<any> {
    return this.http
      .get<RestResponse<any>>(`${environment.apiUrl}/statistic/user-structure`)
      .pipe(map(res => res.data));
  }
}
