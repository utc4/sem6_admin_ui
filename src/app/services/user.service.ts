import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/user.model';
import {PageResponse} from '../dto/page-response';
import {environment} from '../../environments/environment';
import {RestResponse} from '../dto/rest-response';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  constructor(private http: HttpClient) { }

  list(page: number = 1, limit: number = 5): Observable<PageResponse<User[]>> {
    const params = new HttpParams()
      .set('limit', limit.toString())
      .set('page', page.toString());
    return this.http.get<PageResponse<User[]>>(`${environment.apiUrl}/user`, {params: params});
  }

  one(id: number): Observable<User> {
    return this.http
      .get<RestResponse<User>>(`${environment.apiUrl}/user/${id}`)
      .pipe(
        map(rs => rs.data),
      );
  }

  activeUser(id: number): Observable<User> {
    return this.http
      .put<RestResponse<User>>(`${environment.apiUrl}/user/active/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }

  inactiveUser(id: number): Observable<User> {
    return this.http
      .put<RestResponse<User>>(`${environment.apiUrl}/user/inactive/${id}`, null)
      .pipe(
        map(rs => rs.data),
      );
  }
}
